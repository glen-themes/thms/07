![Text banner that says "Wonder Duo" in uppercase, and "Theme 07 by glenthemes" at the bottom](https://64.media.tumblr.com/ed8b1dce75eaf8ff1c9517997c94c0e4/cfcf8b4ab07e6f50-c3/s1280x1920/8eb80b7e486f776bb6ace6da1b40bb00d655ada4.png)  
![Screenshot preview of the theme "Wonder Duo" by glenthemes](https://64.media.tumblr.com/efedef3be83f3122642345947c7ac2a0/cfcf8b4ab07e6f50-15/s1280x1920/59bcb8a6117f9f4c0df1aea14ee05086f5a621ff.gif)

**Theme no.:** 07  
**Theme name:** Wonder Duo  
**Theme type:** Free / Tumblr use  
**Description:** A header and sidebar theme featuring Bakugou Katsuki and Midoriya Izuku from Boku no Hero Academia.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-09-04](https://64.media.tumblr.com/89db0e83940c83672258ddd789811a53/tumblr_nu5ujfHzvO1ubolzro1_540.gif)  
**Rework date [v1]:** [2017-01-21](https://64.media.tumblr.com/714862a519985aea75a7581dbbf94f2d/tumblr_nu5ujfHzvO1ubolzro2_r1_540.gif)  
**Rework date [v2]:** 2021-10-28

**Post:** [glenthemes.tumblr.com/post/156182782424](https://glenthemes.tumblr.com/post/156182782424)  
**Preview:** [glenthpvs.tumblr.com/wonderduo](https://glenthpvs.tumblr.com/wonderduo)  
**Download:** [pastebin.com/0BQCnLFR](https://pastebin.com/0BQCnLFR)  
**Credits:** [glencredits.tumblr.com/wonderduo](https://glencredits.tumblr.com/wonderduo)
